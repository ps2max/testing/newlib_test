#include <stdio.h>
#include <malloc.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>

#include <sbv_patches.h>
#include <debug.h>
#include <sifrpc.h>
#include <iopcontrol.h>
#include <iopheap.h>

#define NEWLIB_PORT_AWARE
#include <iox_stat.h>
#include <io_common.h>


#define NEWLIB_PORT

#define PRINTF printf
//#define PRINTF scr_printf

#define IRX_DEFINE(mod) \
extern unsigned char mod##_irx[]; \
extern unsigned int size_##mod##_irx

#define IRX_LOAD(mod) \
    if (SifExecModuleBuffer(mod##_irx, size_##mod##_irx, 0, NULL, NULL) < 0) \
        PRINTF("Could not load '%s'\n", #mod)

IRX_DEFINE(usbd);
IRX_DEFINE(usbhdfsd);
IRX_DEFINE(bdm);
IRX_DEFINE(bdmfs_vfat);
IRX_DEFINE(usbmass_bd);
IRX_DEFINE(iomanX);
IRX_DEFINE(fileXio);


int test_malloc()
{
	int i;
	void * data = NULL;

	PRINTF("malloc test:\n");

	for (i=0; i<10; i++) {
		data = malloc(i*1000);
		if (data == NULL) {
			PRINTF("- malloc failed\n");
			return -1;
		}
		PRINTF("- malloc returned 0x%x\n", data);
		free(data);
	}

	PRINTF("succes\n\n");
	return 0;
}

int test_clock()
{
	int i;
	clock_t tEnd;

	PRINTF("clock test:\n");
	PRINTF("- sizeof(clock_t)=%d\n", sizeof(clock_t));

	for (i=0; i<10; i++) {
		tEnd = clock() + CLOCKS_PER_SEC;
		while (clock() < tEnd) {
		}
		PRINTF("- 1s passed...\n");
	}

	PRINTF("succes\n\n");
	return 0;
}

int test_sleep()
{
	int i;

	PRINTF("sleep test:\n");

	for (i=0; i<10; i++) {
		sleep(1);
		PRINTF("- 1s passed...\n");
	}

	PRINTF("succes\n\n");
	return 0;
}

#ifdef NEWLIB_PORT
int test_nanosleep()
{
	clock_t tBegin, tDiff;
	struct timespec ts;
	int i;

	PRINTF("nanosleep test:\n");

	for (i=0; i<10; i++) {
		ts.tv_sec = 1;
		ts.tv_nsec = 0;
		tBegin = clock();
		nanosleep(&ts, &ts);
		tDiff = clock() - tBegin;
		PRINTF("- 1s sleep requested, lasted %dus\n", (tDiff*1000000ULL)/CLOCKS_PER_SEC);
	}

	PRINTF("succes\n\n");
	return 0;
}

int test_smalltimes()
{
	clock_t tBegin, tDiff;
	struct timespec ts;
	unsigned long long us;

	PRINTF("smaltimes test:\n");

	for (us=1; us <= 1000000ULL; us *= 10ULL) {
		ts.tv_sec = 0;
		ts.tv_nsec = us * 1000ULL;
		tBegin = clock();
		nanosleep(&ts, &ts);
		tDiff = clock() - tBegin;
		PRINTF("- %dus sleep requested, lasted %dus\n", us, (tDiff*1000000ULL)/CLOCKS_PER_SEC);
	}

	PRINTF("succes\n\n");
	return 0;
}

int test_dirent()
{
	const char *dirname = "mass:";
	struct dirent *pDirent;
	DIR *pDir;

	PRINTF("dirent test:\n");

	pDir = opendir(dirname);
	if (pDir == NULL) {
		PRINTF ("Cannot open directory '%s'\n", dirname);
		return -1;
	}

	while ((pDirent = readdir(pDir)) != NULL) {
                struct stat st;
                char filename[80];
                mode_t m;
                struct tm * timeinfo;

                snprintf(filename, 80, "%s/%s", dirname, pDirent->d_name);
                if (stat(filename, &st) < 0) {
                        PRINTF ("%s\n", filename);
                        continue;
                }
                m = st.st_mode;
                timeinfo = localtime(&st.st_mtime);

                PRINTF ("%c%c%c%c%c%c%c%c%c%c %s %6llu %s\n",
                        (m & S_IFDIR) != 0 ? 'd' : '-',
                        (m & S_IRUSR) != 0 ? 'r' : '-',
                        (m & S_IWUSR) != 0 ? 'w' : '-',
                        (m & S_IXUSR) != 0 ? 'x' : '-',
                        (m & S_IRGRP) != 0 ? 'r' : '-',
                        (m & S_IWGRP) != 0 ? 'w' : '-',
                        (m & S_IXGRP) != 0 ? 'x' : '-',
                        (m & S_IROTH) != 0 ? 'r' : '-',
                        (m & S_IWOTH) != 0 ? 'w' : '-',
                        (m & S_IXOTH) != 0 ? 'x' : '-',
                        asctime(timeinfo),
                        st.st_size,
                        pDirent->d_name);
	}
	closedir (pDir);
	return 0;
}
#endif

int test_fio()
{
	const char *dirname = "mass:";
	io_dirent_t dir;
	int fd;

	PRINTF("fio test: (tz=%d, ds=%d)\n", configGetTimezone(), configIsDaylightSavingEnabled());

	fd = fioDopen(dirname);
	if (fd <= 0) {
		PRINTF ("  Cannot open directory '%s'\n", dirname);
		return -1;
	}

	while (fioDread(fd, &dir) > 0) {
                PRINTF ("%c%c%c%c %2d-%02d-%04d %2d:%02d:%02d %6llu %s\n",
                        (dir.stat.mode & FIO_SO_IFDIR) != 0 ? 'd' : '-',
                        (dir.stat.mode & FIO_SO_IROTH) != 0 ? 'r' : '-',
                        (dir.stat.mode & FIO_SO_IWOTH) != 0 ? 'w' : '-',
                        (dir.stat.mode & FIO_SO_IXOTH) != 0 ? 'x' : '-',
                        dir.stat.mtime[4], dir.stat.mtime[5], (u16)dir.stat.mtime[6] | ((u16)dir.stat.mtime[7] << 8),
                        dir.stat.mtime[3], dir.stat.mtime[2], dir.stat.mtime[1],
                        (u64)dir.stat.size | ((u64)dir.stat.hisize << 32),
                        dir.name);
	}
	fioDclose(fd);
	return 0;
}

int test_fileXio()
{
	const char *dirname = "mass:";
	iox_dirent_t dir;
	int fd;

	PRINTF("fileXio test:\n");

	fd = fileXioDopen(dirname);
	if (fd <= 0) {
		PRINTF ("  Cannot open directory '%s'\n", dirname);
		return -1;
	}

	while (fileXioDread(fd, &dir) > 0) {
                PRINTF ("%c%c%c%c%c%c%c%c%c%c %2d-%02d-%04d %2d:%02d:%02d %6llu %s\n",
                        (dir.stat.mode & FIO_S_IFDIR) != 0 ? 'd' : '-',
                        (dir.stat.mode & FIO_S_IRUSR) != 0 ? 'r' : '-',
                        (dir.stat.mode & FIO_S_IWUSR) != 0 ? 'w' : '-',
                        (dir.stat.mode & FIO_S_IXUSR) != 0 ? 'x' : '-',
                        (dir.stat.mode & FIO_S_IRGRP) != 0 ? 'r' : '-',
                        (dir.stat.mode & FIO_S_IWGRP) != 0 ? 'w' : '-',
                        (dir.stat.mode & FIO_S_IXGRP) != 0 ? 'x' : '-',
                        (dir.stat.mode & FIO_S_IROTH) != 0 ? 'r' : '-',
                        (dir.stat.mode & FIO_S_IWOTH) != 0 ? 'w' : '-',
                        (dir.stat.mode & FIO_S_IXOTH) != 0 ? 'x' : '-',
                        dir.stat.mtime[4], dir.stat.mtime[5], (u16)dir.stat.mtime[6] | ((u16)dir.stat.mtime[7] << 8),
                        dir.stat.mtime[3], dir.stat.mtime[2], dir.stat.mtime[1],
                        (u64)dir.stat.size | ((u64)dir.stat.hisize << 32),
                        dir.name);
	}
	fileXioDclose(fd);
	return 0;
}

int test_time()
{
	PRINTF("time test:\n");

        time_t rawtime;
        struct tm * timeinfo;

        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        PRINTF ( "Current local time and date: %s", asctime (timeinfo) );

	return 0;
}

int test_fread()
{
        const char *filename = "mass:hello.txt";
        char text[81];
        FILE *fp;
        int size;

        PRINTF("fread test:\n");

        fp = fopen(filename, "rb");
        if (fp == NULL) {
                PRINTF ("  Cannot open file '%s'\n", filename);
		return -1;
        }

        fseek(fp, 0, SEEK_END);
        size = ftell(fp);
        PRINTF ("file '%s', size=%d\n", filename, size);

        fseek(fp, 0, SEEK_SET);
        while ((size = fread(text, 1, 10, fp)) >= 1) {
                text[size] = 0;
                PRINTF("fread%d: %s\n", size, text);
        }

        fseek(fp, 0, SEEK_SET);
        while ((size = fread(text, 1, 10, fp)) >= 1) {
                text[size] = 0;
                PRINTF("fread%d: %s\n", size, text);
        }

        fclose(fp);
        return 0;
}

int test_read()
{
        const char *filename = "mass:hello.txt";
        char text[81];
        int fd;
        int size;

        PRINTF("read test:\n");

        fd = open(filename, O_RDONLY);
        if (fd <= 0) {
                PRINTF ("  Cannot open file '%s'\n", filename);
		return -1;
        }

        while ((size = read(fd, text, 10)) >= 1) {
                text[size] = 0;
                PRINTF("read%d: %s\n", size, text);
        }

        close(fd);
        return 0;
}

int test_fioRead()
{
        const char *filename = "mass:hello.txt";
        char text[81];
        int fd;
        int size;

        PRINTF("fioRead test:\n");

        fd = fioOpen(filename, FIO_O_RDONLY);
        if (fd <= 0) {
                PRINTF ("  Cannot open file '%s'\n", filename);
		return -1;
        }

        while ((size = fioRead(fd, text, 10)) >= 1) {
                text[size] = 0;
                PRINTF("read%d: %s\n", size, text);
        }

        fioClose(fd);
        return 0;
}

int main()
{
	PRINTF("newlib testing...\n\n");

#if 0
	SifInitRpc(0);

	//Reboot IOP
	while(!SifIopReset("", 0)){};
	while(!SifIopSync()){};

	//Initialize SIF services
	SifInitRpc(0);
	SifLoadFileInit();
	SifInitIopHeap();
	sbv_patch_enable_lmb();
#endif

	IRX_LOAD(iomanX);
	IRX_LOAD(fileXio);
	IRX_LOAD(usbd);
//        IRX_LOAD(usbhdfsd);
        IRX_LOAD(bdm);
        IRX_LOAD(bdmfs_vfat);
        IRX_LOAD(usbmass_bd);
	sleep(3);

        // "fio" file and directory tests
        test_fioRead();
        test_read();
        test_fread();
        test_fio();
#ifdef NEWLIB_PORT
        test_dirent();
#endif

        fileXioInit();

        // "fileXio" file and directory tests
        test_read();
        test_fread();
        test_fileXio();
#ifdef NEWLIB_PORT
        test_dirent();
#endif

#ifdef NEWLIB_PORT
        test_time();
	test_smalltimes();
	test_nanosleep();
#endif
	test_sleep();
	test_clock();
	test_malloc();

//	while(1){}

	return 0;
}
