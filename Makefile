EE_BIN = hello.elf
EE_OBJS = hello.o
EE_LIBS = -lfileXio -lcdvd

# Add embedded IRX files
EE_IRX_FILES=\
	usbd.irx \
	usbhdfsd.irx \
	bdm.irx \
	bdmfs_vfat.irx \
	usbmass_bd.irx \
	iomanX.irx \
	fileXio.irx
EE_IRX_SRCS = $(addsuffix _irx.c, $(basename $(EE_IRX_FILES)))
EE_IRX_OBJS = $(addsuffix _irx.o, $(basename $(EE_IRX_FILES)))
EE_OBJS += $(EE_IRX_OBJS)

# Where to find the IRX files
vpath %.irx $(PS2SDK)/iop/irx/

# Rule to generate them
%_irx.o: %.irx
	bin2c $< $*_irx.c $*_irx
	$(EE_CC) -c $*_irx.c -o $*_irx.o

# This is for the sbv patch
SBVLITE = $(PS2SDK)/sbv
EE_INCS += -I$(SBVLITE)/include
EE_LDFLAGS += -L$(SBVLITE)/lib
EE_LIBS += -lpatches

all: $(EE_BIN)

clean:
	rm -f $(EE_BIN) $(EE_OBJS) $(EE_IRX_SRCS)

sim: $(EE_BIN)
	PCSX2 --elf=${PWD}/$(EE_BIN) --nogui

run: $(EE_BIN)
	ps2client -h 192.168.1.10 execee host:$(EE_BIN)

include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
